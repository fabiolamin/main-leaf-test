using UnityEngine;
using Cinemachine;
using Scripts.Input;

namespace Scripts.Cameras
{
    public sealed class FirstPersonCamera : CinemachineExtension
    {
        [SerializeField] private InputManager _inputManager;

        [SerializeField] private float _verticalAngle = 80f;
        [SerializeField] private float _verticalSensibility = 5f;
        [SerializeField] private float _horizontalSensibility = 5f;
        private Vector3 _rotation;

        protected override void PostPipelineStageCallback(CinemachineVirtualCameraBase vcam, CinemachineCore.Stage stage, ref CameraState state, float deltaTime)
        {
            if (vcam.Follow)
            {
                if (stage == CinemachineCore.Stage.Aim)
                {
                    Vector2 mouseDelta = _inputManager.MouseDelta;
                    _rotation.x += mouseDelta.x * Time.deltaTime * _verticalSensibility;
                    _rotation.y += mouseDelta.y * Time.deltaTime * _horizontalSensibility;
                    _rotation.y = Mathf.Clamp(_rotation.y, -_verticalAngle, _verticalAngle);
                    state.RawOrientation = Quaternion.Euler(-_rotation.y, _rotation.x, 0f);
                }
            }
        }
    }
}

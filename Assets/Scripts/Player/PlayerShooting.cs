using UnityEngine;
using Scripts.Weapons;
using Scripts.Utils;
using System.Collections;

namespace Scripts.Player
{
    public sealed class PlayerShooting : MonoBehaviour
    {
        [SerializeField] private Camera _fpsCamera;
        [SerializeField] private Transform _weaponLocation;
        [SerializeField] private ObjectPooling _projectilePooling;

        private RangeWeapon _rangeWeapon;
        private Projectile _currentProjectile;
        private int _currentAmmoAmount;
        private bool _isRealoding = false;

        public delegate void AmmoUpdatedHandler(int currentAmmoAmount, int totalAmmoAmount);
        public event AmmoUpdatedHandler OnAmmoUpdated;

        public void Initialize(RangeWeapon rangeWeapon)
        {
            _rangeWeapon = Instantiate(rangeWeapon, _weaponLocation.position, _weaponLocation.localRotation, _weaponLocation);
            _currentAmmoAmount = _rangeWeapon.RangeWeaponData.AmmoAmount;

            OnAmmoUpdated?.Invoke(_currentAmmoAmount, _rangeWeapon.RangeWeaponData.AmmoAmount);
            _projectilePooling.StartPooling();

            GetProjectile();
        }

        public void Shoot(bool hasShot)
        {
            if (CanShoot(hasShot))
            {
                _currentProjectile.GetComponent<Collider>().enabled = true;
                _currentProjectile.transform.SetParent(null);
                _currentProjectile.MoveTowards(GetAim());
                _currentProjectile = null;
                UpdateAmmo(-1);
            }
        }

        public void UpdateAmmo(int amount)
        {
            _currentAmmoAmount = Mathf.Clamp(_currentAmmoAmount + amount, 0, _rangeWeapon.RangeWeaponData.AmmoAmount);
            OnAmmoUpdated?.Invoke(_currentAmmoAmount, _rangeWeapon.RangeWeaponData.AmmoAmount);
            StartCoroutine(ReloadProjectile());
        }

        private void GetProjectile()
        {
            GameObject newProjectile = _projectilePooling.GetPooledGameObject();

            if (newProjectile != null)
            {
                newProjectile.GetComponent<Rigidbody>().useGravity = false;
                newProjectile.GetComponent<Collider>().enabled = false;
                _currentProjectile = newProjectile.GetComponent<Projectile>();
            }
        }

        private bool CanShoot(bool hasShot)
        {
            return hasShot && !_isRealoding && _currentAmmoAmount > 0;
        }

        private Vector3 GetAim()
        {
            float x = Screen.width / 2;
            float y = Screen.height / 2;
            Vector3 screenCenter = new Vector3(x, y, 0);
            Ray ray = _fpsCamera.ScreenPointToRay(screenCenter);

            return ray.direction;
        }

        private IEnumerator ReloadProjectile()
        {
            if (_currentAmmoAmount > 0 && _currentProjectile == null)
            {
                GetProjectile();
                _currentProjectile.gameObject.SetActive(false);
                _isRealoding = true;

                yield return new WaitForSeconds(_rangeWeapon.RangeWeaponData.ReloadingDelay);

                _currentProjectile.gameObject.SetActive(true);
                _isRealoding = false;
            }
        }
    }
}

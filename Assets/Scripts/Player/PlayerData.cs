using UnityEngine;
using Scripts.Weapons;

namespace Scripts.Player
{
    [CreateAssetMenu(fileName = "Player Data", menuName = "New Player Data")]
    public class PlayerData : ScriptableObject
    {
        [SerializeField] private int _maxHealth;
        [SerializeField] private float _movementSpeed;
        [SerializeField] private float _jumpForce;
        [SerializeField] private RangeWeapon _rangeWeapon;

        public int MaxHealth => _maxHealth;
        public float MovementSpeed => _movementSpeed;
        public float JumpForce => _jumpForce;
        public RangeWeapon RangeWeapon => _rangeWeapon;
    }
}


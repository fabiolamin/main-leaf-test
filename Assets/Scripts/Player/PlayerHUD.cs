using UnityEngine;
using UnityEngine.UI;

namespace Scripts.Player
{
    public sealed class PlayerHUD : MonoBehaviour
    {
        [SerializeField] private Text _ammoText;
        [SerializeField] private Text _healthText;
        [SerializeField] private Text _scoreText;
        [SerializeField] private Animator _screenDamageAnimator;
        
        public void UpdateAmmoText(int currentAmmoAmount, int totalAmmoAmount)
        {
            _ammoText.text = currentAmmoAmount + "/" + totalAmmoAmount;
        }

        public void UpdateHealthText(int currentHealth)
        {
            _healthText.text = currentHealth.ToString();
        }

        public void TriggerScreenDamage()
        {
            _screenDamageAnimator.gameObject.SetActive(true);
            _screenDamageAnimator.SetTrigger("Damage");
        }

        public void UpdateScoreText(int score)
        {
            _scoreText.text = score.ToString();
        }
    }
}


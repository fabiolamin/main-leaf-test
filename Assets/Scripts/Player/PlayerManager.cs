using UnityEngine;

namespace Scripts.Player
{
    public sealed class PlayerManager : MonoBehaviour
    {
        [SerializeField] private PlayerController _playerController;

        public PlayerController PlayerController => _playerController;

        public void OnAwake()
        {
            _playerController.Initialize();
        }
    }
}


using UnityEngine;

namespace Scripts.Player
{
    public sealed class PlayerMovement : MonoBehaviour
    {
        [SerializeField] private Rigidbody _playerRigidbody;
        [SerializeField] private Transform _fpsCamera;

        private float _movementSpeed;
        private float _jumpForce;
        private Vector3 _movement;

        public void Initialize(float movementSpeed, float jumpForce)
        {
            _movementSpeed = movementSpeed;
            _jumpForce = jumpForce;
        }

        public void Move(Vector2 axis)
        {
            _movement = _playerRigidbody.velocity;
            _movement.x = axis.x * _movementSpeed;
            _movement.z = axis.y * _movementSpeed;

            _movement = _fpsCamera.forward * _movement.z + _fpsCamera.right * _movement.x;
            _movement.y = _playerRigidbody.velocity.y;
            _playerRigidbody.velocity = _movement;
        }

        public void Jump(bool hasJumped)
        {
            if (hasJumped && IsOnTheGround())
            {
                _movement = _playerRigidbody.velocity;
                _movement.y = _jumpForce;
                _playerRigidbody.velocity = _movement;
            }
        }

        private bool IsOnTheGround()
        {
            return Physics.Raycast(transform.position, Vector3.down, 1f);
        }
    }
}


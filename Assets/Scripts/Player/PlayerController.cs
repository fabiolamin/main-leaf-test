using UnityEngine;
using Scripts.Utils;

namespace Scripts.Player
{
    public sealed class PlayerController : MonoBehaviour
    {
        [SerializeField] private PlayerData _playerData;
        [SerializeField] private PlayerMovement _playerMovement;
        [SerializeField] private PlayerShooting _playerShooting;
        [SerializeField] private Health _health;
        [SerializeField] private PlayerHUD _playerHUD;

        private int _score = 0;
        private int _defeatedEnemies = 0;

        public delegate void ScoreUpdatedHandler(int score);
        public event ScoreUpdatedHandler OnScoreUpdated;

        public PlayerMovement PlayerMovement => _playerMovement;
        public PlayerShooting PlayerShooting => _playerShooting;

        public void Initialize()
        {
            OnScoreUpdated += _playerHUD.UpdateScoreText;
            _playerShooting.OnAmmoUpdated += _playerHUD.UpdateAmmoText;
            _health.OnHealthDamaged += _playerHUD.TriggerScreenDamage;
            _health.OnHealthUpdated += _playerHUD.UpdateHealthText;

            _playerMovement.Initialize(_playerData.MovementSpeed, _playerData.JumpForce);
            _health.Initialize(_playerData.MaxHealth);
            _playerShooting.Initialize(_playerData.RangeWeapon);

            OnScoreUpdated?.Invoke(_score);
        }

        public void UpdateScore(int value)
        {
            _defeatedEnemies++;
            _score += value;
            OnScoreUpdated?.Invoke(_score);
        }
    }
}


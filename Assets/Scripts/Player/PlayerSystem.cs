using UnityEngine;
using Scripts.Input;
using Scripts.Enemy;

namespace Scripts.Player
{
    public sealed class PlayerSystem : MonoBehaviour
    {
        [SerializeField] private InputManager _inputManager;
        [SerializeField] private PlayerManager _playerManager;
        [SerializeField] private EnemyManager _enemyManager;

        private void Awake()
        {
            _playerManager.OnAwake();

            _inputManager.OnMovementAxisUpdated += _playerManager.PlayerController.PlayerMovement.Move;
            _inputManager.OnJumpButtonPressed += _playerManager.PlayerController.PlayerMovement.Jump;
            _inputManager.OnShootButtonPressed += _playerManager.PlayerController.PlayerShooting.Shoot;

            foreach(EnemyController enemyController in _enemyManager.EnemyControllers)
            {
                enemyController.OnPointsDropped += _playerManager.PlayerController.UpdateScore;
            }
        }

        private void Update()
        {
            _inputManager.OnUpdate();
        }
    }
}


using UnityEngine;
using Scripts.Utils;

namespace Scripts.Weapons
{
    public sealed class Projectile : MonoBehaviour
    {
        [SerializeField] private ProjectileData _projectileData;
        [SerializeField] private Rigidbody _projectileRigidbody;

        public ProjectileData ProjectileData => _projectileData;

        public void MoveTowards(Vector3 direction)
        {
            _projectileRigidbody.useGravity = true;
            float speed = _projectileData.Speed;
            _projectileRigidbody.velocity = direction * speed;
        }

        private void OnCollisionEnter(Collision collision)
        {
            Health health = collision.gameObject.GetComponent<Health>();

            if (health != null)
            {
                health.GetDamage(ProjectileData.Damage);
            }

            gameObject.SetActive(false);
            _projectileRigidbody.velocity = Vector3.zero;
        }
    }
}


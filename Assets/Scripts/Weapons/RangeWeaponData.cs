using UnityEngine;

namespace Scripts.Weapons
{
    [CreateAssetMenu(fileName = "Range Weapon Data", menuName = "New Range Weapon Data")]
    public class RangeWeaponData : ScriptableObject
    {
        [SerializeField] private int _ammoAmount;
        [SerializeField] private float _reloadingDelay;

        public int AmmoAmount => _ammoAmount;
        public float ReloadingDelay => _reloadingDelay;
    }
}


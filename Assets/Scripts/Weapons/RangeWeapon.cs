using UnityEngine;

namespace Scripts.Weapons
{
    public sealed class RangeWeapon : Weapon
    {
        [SerializeField] private RangeWeaponData _rangeWeaponData;

        public RangeWeaponData RangeWeaponData => _rangeWeaponData;

        public override void Initialize()
        {
            Damage = 0;
        }
    }
}


using UnityEngine;

namespace Scripts.Weapons
{
    [CreateAssetMenu(fileName = "Melee Weapon Data", menuName = "New Melee Weapon Data")]
    public class MeleeWeaponData : ScriptableObject
    {
        [SerializeField] private int _damage;

        public int Damage => _damage;
    }
}


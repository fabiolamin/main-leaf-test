using UnityEngine;

namespace Scripts.Weapons
{
    public abstract class Weapon : MonoBehaviour
    {
        public int Damage { get; protected set; } = 0;

        public abstract void Initialize();
    }
}

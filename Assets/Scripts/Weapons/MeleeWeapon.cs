using UnityEngine;

namespace Scripts.Weapons
{
    public class MeleeWeapon : Weapon
    {
        [SerializeField] private MeleeWeaponData _meleeWeaponData;

        public MeleeWeaponData MeleeWeaponData => _meleeWeaponData;

        public override void Initialize()
        {
            Damage = MeleeWeaponData.Damage;
        }
    }
}


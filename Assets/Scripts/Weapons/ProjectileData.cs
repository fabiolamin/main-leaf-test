using UnityEngine;

namespace Scripts.Weapons
{
    [CreateAssetMenu(fileName = "Projectile Data", menuName = "New Projectile Data")]
    public class ProjectileData : ScriptableObject
    {
        [SerializeField] private float _speed;
        [SerializeField] private int _damage;

        public float Speed => _speed;
        public int Damage => _damage;
    }
}


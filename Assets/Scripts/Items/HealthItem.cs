using UnityEngine;
using Scripts.Utils;

namespace Scripts.Items
{
    public sealed class HealthItem : Item
    {
        [SerializeField] private HealthItemData _healthItemData;
        protected override void GetBonus(Collider player)
        {
            Health playerHealth = player.GetComponent<Health>();
            playerHealth.UpdateHealth(_healthItemData.HealthAmount);
        }
    }
}


using UnityEngine;

namespace Scripts.Items
{
    [CreateAssetMenu(fileName = "Ammo Item Data", menuName = "New Ammo Item Data")]
    public sealed class AmmoItemData : ScriptableObject
    {
        [SerializeField] private int _ammoAmount;

        public int AmmoAmount => _ammoAmount;
    }
}


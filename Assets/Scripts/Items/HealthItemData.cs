using UnityEngine;

namespace Scripts.Items
{
    [CreateAssetMenu(fileName = "Health Item Data", menuName = "New Health Item Data")]
    public sealed class HealthItemData : ScriptableObject
    {
        [SerializeField] private int _healthAmount;

        public int HealthAmount => _healthAmount;
    }
}


using UnityEngine;
using Scripts.Utils;

namespace Scripts.Items
{
    public sealed class ItemSpawner : MonoBehaviour
    {
        [SerializeField] private ObjectPooling[] _objectPoolings;

        public void Initialize()
        {
            foreach (ObjectPooling objectPooling in _objectPoolings)
            {
                objectPooling.StartPooling();
            }
        }

        public void SpawnRandomItem(Vector3 position)
        {
            int random = Random.Range(0, _objectPoolings.Length);

            GameObject item = _objectPoolings[random].GetPooledGameObject();
            item.transform.SetParent(null);

            item.transform.position = position + Vector3.up * 2;
        }
    }
}


using UnityEngine;
using Scripts.Player;

namespace Scripts.Items
{
    public sealed class AmmoItem : Item
    {
        [SerializeField] private AmmoItemData _ammoItemData;

        protected override void GetBonus(Collider player)
        {
            PlayerShooting playerShooting = player.GetComponent<PlayerShooting>();
            playerShooting.UpdateAmmo(_ammoItemData.AmmoAmount);
        }
    }
}


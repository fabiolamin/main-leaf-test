using UnityEngine;

namespace Scripts.Items
{
    public abstract class Item : MonoBehaviour
    {
        protected abstract void GetBonus(Collider player);

        private void OnTriggerEnter(Collider player)
        {
            GetBonus(player);
            gameObject.SetActive(false);
        }
    }
}


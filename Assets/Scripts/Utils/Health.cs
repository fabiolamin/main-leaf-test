using UnityEngine;

namespace Scripts.Utils
{
    public sealed class Health : MonoBehaviour
    {
        private int _maxHealth;
        private int _currentHealth;

        public delegate void HealthUpdatedHandler(int currentHealth);
        public event HealthUpdatedHandler OnHealthUpdated;

        public delegate void HealthDamagedHandler();
        public event HealthDamagedHandler OnHealthDamaged;

        public delegate void DeathTriggeredHandler();
        public event DeathTriggeredHandler OnDeathTriggered;

        public void Initialize(int maxHealth)
        {
            _maxHealth = maxHealth;
            _currentHealth = maxHealth;
            OnHealthUpdated?.Invoke(maxHealth);
        }

        public void GetDamage(int amount)
        {
            UpdateHealth(-amount);
            OnHealthDamaged?.Invoke();
            CheckHealth();
        }

        public void UpdateHealth(int amount)
        {
            _currentHealth = Mathf.Clamp(_currentHealth + amount, 0, _maxHealth);
            OnHealthUpdated?.Invoke(_currentHealth);
        }

        private void CheckHealth()
        {
            if(_currentHealth == 0)
            {
                SetDeath();
            }
        }

        private void SetDeath()
        {
            OnDeathTriggered?.Invoke();
        }
    }
}


using UnityEngine;

namespace Scripts.Utils
{
    public abstract class BaseState : MonoBehaviour
    {
        [SerializeField] protected StateMachine _stateMachine;
        [SerializeField] protected BaseState nextState;

        public abstract void SetUpState();
        public abstract void UpdateState();
    }
}


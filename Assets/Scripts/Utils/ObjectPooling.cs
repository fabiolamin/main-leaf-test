using System.Collections.Generic;
using UnityEngine;

namespace Scripts.Utils
{
    public sealed class ObjectPooling : MonoBehaviour
    {
        [SerializeField] private GameObject _pooledGameObject;
        [SerializeField] private int _pooledAmount;
        [SerializeField] private Transform _pooledLocation;

        private List<GameObject> pooledGameObjects = new List<GameObject>();

        public void StartPooling()
        {
            for (int i = 0; i < _pooledAmount; i++)
            {
                InstantiatePooledGameObject();
            }
        }

        public GameObject GetPooledGameObject()
        {
            foreach (GameObject pooledGameObject in pooledGameObjects)
            {
                if (!pooledGameObject.activeSelf)
                {
                    SetUpPooledGameObject(pooledGameObject);
                    return pooledGameObject;
                }
            }

            return null;
        }

        private void InstantiatePooledGameObject()
        {
            GameObject pooledGameObject = Instantiate(_pooledGameObject, _pooledLocation.position, _pooledLocation.rotation, _pooledLocation);
            pooledGameObject.SetActive(false);
            pooledGameObjects.Add(pooledGameObject);
        }

        private void SetUpPooledGameObject(GameObject pooledGameObject)
        {
            pooledGameObject.SetActive(true);
            pooledGameObject.transform.parent = _pooledLocation;
            pooledGameObject.transform.localPosition = _pooledLocation.localPosition;
            pooledGameObject.transform.localRotation = _pooledLocation.localRotation;
        }
    }
}


using UnityEngine;

namespace Scripts.Utils
{
    public sealed class StateMachine : MonoBehaviour
    {
        [SerializeField] private BaseState _currentState;

        public void Initialize()
        {
            _currentState.SetUpState();
        }

        public void UpdateState()
        {
            if (_currentState != null)
            {
                _currentState.UpdateState();
            }
        }

        public void ChangeState(BaseState newState)
        {
            _currentState = newState;
            _currentState.SetUpState();
        }
    }
}

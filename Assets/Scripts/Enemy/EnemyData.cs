using UnityEngine;
using Scripts.Weapons;

[CreateAssetMenu(fileName = "Enemy Data", menuName = "New Enemy Data")]
public class EnemyData : ScriptableObject
{
    [SerializeField] private int _maxHealth;
    [SerializeField] private float _movementSpeed;
    [SerializeField] private int _points;
    [SerializeField] private Weapon _firstWeapon;
    [SerializeField] private Weapon _secondWeapon;
    [SerializeField] private Material _armorColor;

    public int MaxHealth => _maxHealth;
    public float MovementSpeed => _movementSpeed;
    public int Points => _points;
    public Weapon FirstWeapon => _firstWeapon;
    public Weapon SecondWeapon => _secondWeapon;
    public Material ArmorColor => _armorColor;
}

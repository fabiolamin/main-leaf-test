using UnityEngine;
using UnityEngine.AI;
using Scripts.Utils;
using Scripts.Items;
using System.Collections;

namespace Scripts.Enemy
{
    public sealed class EnemyController : MonoBehaviour
    {
        [SerializeField] private NavMeshAgent _enemyAgent;
        [SerializeField] private Animator _enemyAnimator;
        [SerializeField] private Collider _enemyCollider;
        [SerializeField] private EnemyAttack _enemyAttack;
        [SerializeField] private Health _enemyHealth;
        [SerializeField] private EnemyData _enemyData;
        [SerializeField] private StateMachine _enemyStateMachine;
        [SerializeField] private SkinnedMeshRenderer _enemyMeshRenderer;
        [SerializeField] private ItemSpawner _itemSpawner;
        [SerializeField] private float _deactivationDelay = 5f;

        private bool _isActivated = true;

        public delegate void PointsDroppedHandler(int value);
        public event PointsDroppedHandler OnPointsDropped;

        public void Initialize()
        {
            _enemyAttack.Initialize();
            _enemyStateMachine.Initialize();
            _enemyHealth.Initialize(_enemyData.MaxHealth);
            _itemSpawner.Initialize();

            _enemyHealth.OnHealthDamaged += TriggerDamageAnimation;
            _enemyHealth.OnDeathTriggered += Deactivate;
            _enemyHealth.OnDeathTriggered += DropItem;
            _enemyHealth.OnDeathTriggered += DropPoints;

            _enemyAgent.speed = _enemyData.MovementSpeed;
            _enemyMeshRenderer.material = _enemyData.ArmorColor;
        }

        public void CheckEnemy()
        {
            if (_isActivated)
            {
                _enemyAnimator.SetBool("IsMoving", !_enemyAgent.isStopped);
                _enemyStateMachine.UpdateState();
            }
        }

        private void TriggerDamageAnimation()
        {
            _enemyAnimator.SetTrigger("Damage");
        }

        private void Deactivate()
        {
            _enemyCollider.enabled = false;
            _isActivated = false;
            _enemyAgent.isStopped = true;
            _enemyAnimator.SetTrigger("Death");
            StartCoroutine(DelayDeactivation());
        }

        private IEnumerator DelayDeactivation()
        {
            yield return new WaitForSeconds(_deactivationDelay);
            gameObject.SetActive(false);
        }

        private void DropItem()
        {
            _itemSpawner.SpawnRandomItem(_enemyAgent.transform.position);
        }

        private void DropPoints()
        {
            OnPointsDropped?.Invoke(_enemyData.Points);
        }
    }
}


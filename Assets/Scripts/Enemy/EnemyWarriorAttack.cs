using UnityEngine;
using Scripts.Weapons;
using Scripts.Utils;
using Scripts.Player;

namespace Scripts.Enemy
{
    public sealed class EnemyWarriorAttack : EnemyAttack
    {
        [SerializeField] private EnemyData enemyData;
        [SerializeField] private Transform _firstHandLocation;
        [SerializeField] private Transform _secondHandLocation;

        private Weapon _firstMeleeWeapon;
        private Weapon _secondMeleeWeapon;

        private Health _playerHealth;

        public override void Initialize()
        {
            _firstMeleeWeapon = InstantiateWeapon(enemyData.FirstWeapon, _firstHandLocation);
            _secondMeleeWeapon = InstantiateWeapon(enemyData.SecondWeapon, _secondHandLocation);

            _playerHealth = FindObjectOfType<PlayerMovement>().GetComponent<Health>();
        }

        public void Hit()
        {
            _playerHealth.GetDamage(_firstMeleeWeapon.Damage);
        }
    }
}


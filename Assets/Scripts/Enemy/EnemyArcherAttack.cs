using UnityEngine;
using Scripts.Weapons;
using Scripts.Utils;
using Scripts.Player;

namespace Scripts.Enemy
{
    public sealed class EnemyArcherAttack : EnemyAttack
    {
        [SerializeField] private EnemyData _enemyData;
        [SerializeField] private Transform _weaponLocation;
        [SerializeField] private ObjectPooling _projectilePooling;

        private PlayerMovement _player;
        private Weapon _rangeWeapon;

        public override void Initialize()
        {
            _player = FindObjectOfType<PlayerMovement>();
            _rangeWeapon = InstantiateWeapon(_enemyData.FirstWeapon, _weaponLocation);
            _projectilePooling.StartPooling();
        }

        public void Shoot()
        {
            GameObject newProjectile = _projectilePooling.GetPooledGameObject();

            if (newProjectile != null)
            {
                newProjectile.GetComponent<Rigidbody>().useGravity = false;
                Projectile currentProjectile = newProjectile.GetComponent<Projectile>();
                currentProjectile = newProjectile.GetComponent<Projectile>();
                currentProjectile.transform.LookAt(_player.transform);
                currentProjectile.transform.SetParent(null);
                currentProjectile.MoveTowards(currentProjectile.transform.forward);
            }
        }
    }
}
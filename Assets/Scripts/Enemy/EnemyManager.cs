using UnityEngine;

namespace Scripts.Enemy
{
    public sealed class EnemyManager : MonoBehaviour
    {
        [SerializeField] private EnemyController[] _enemyControllers;

        public EnemyController[] EnemyControllers => _enemyControllers;

        public void OnAwake()
        {
            foreach (EnemyController enemyController in _enemyControllers)
            {
                enemyController.Initialize();
            }
        }

        public void OnUpdate()
        {
            foreach (EnemyController enemyController in _enemyControllers)
            {
                enemyController.CheckEnemy();
            }
        }
    }
}


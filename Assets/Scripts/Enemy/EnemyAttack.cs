using UnityEngine;
using Scripts.Weapons;

namespace Scripts.Enemy
{
    public abstract class EnemyAttack : MonoBehaviour
    {
        public abstract void Initialize();

        protected Weapon InstantiateWeapon(Weapon weaponPrefab, Transform weaponLocation)
        {
            Weapon weapon = Instantiate(weaponPrefab, weaponLocation.position, weaponLocation.rotation, weaponLocation.parent);
            weapon.transform.localScale = weaponLocation.localScale;

            weapon.Initialize();

            return weapon;
        }
    }
}


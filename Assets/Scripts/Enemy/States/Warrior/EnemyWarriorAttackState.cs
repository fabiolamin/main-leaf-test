using UnityEngine;
using Scripts.Utils;
using UnityEngine.AI;
using Scripts.Player;

public class EnemyWarriorAttackState : BaseState
{
    [SerializeField] private NavMeshAgent _enemyAgent;
    [SerializeField] private float _distanceToAttack = 5f;

    private PlayerMovement _player;

    public override void SetUpState()
    {
        _player = FindObjectOfType<PlayerMovement>();
    }

    public override void UpdateState()
    {

        if (IsReadyToAttack())
        {
            _enemyAgent.transform.LookAt(_player.transform);
            _enemyAgent.isStopped = true;
        }
        else
        {
            _stateMachine.ChangeState(nextState);
        }
    }

    private bool IsReadyToAttack()
    {
        float playerDistance = Vector3.Distance(_enemyAgent.transform.position, _player.transform.position);

        return playerDistance <= _distanceToAttack;
    }
}

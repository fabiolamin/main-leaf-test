using UnityEngine;
using Scripts.Utils;
using UnityEngine.AI;
using Scripts.Player;

namespace Scripts.Enemy.States.Warrior
{
    public class EnemyChaseState : BaseState
    {
        [SerializeField] private NavMeshAgent _enemyAgent;

        private PlayerMovement _player;

        public override void SetUpState()
        {
            _player = FindObjectOfType<PlayerMovement>();
            _enemyAgent.isStopped = false;
            _enemyAgent.SetDestination(_player.transform.position);
        }

        public override void UpdateState()
        {
            _stateMachine.ChangeState(nextState);
        }
    }
}


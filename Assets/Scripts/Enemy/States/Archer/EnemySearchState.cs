using UnityEngine;
using UnityEngine.AI;
using Scripts.Player;
using Scripts.Utils;

namespace Scripts.Enemy.States.Archer
{
    public sealed class EnemySearchState : BaseState
    {
        [SerializeField] private NavMeshAgent _enemyAgent;
        [SerializeField] private float _targetRadius = 4f;

        private PlayerMovement _player;

        public override void SetUpState()
        {
            _enemyAgent.isStopped = false;
            _player = FindObjectOfType<PlayerMovement>();
        }

        public override void UpdateState()
        {
            NavMeshHit hit;
            NavMesh.SamplePosition(GetNewLocation(), out hit, _targetRadius, 1);

            _enemyAgent.SetDestination(hit.position);

            _stateMachine.ChangeState(nextState);
        }

        private Vector3 GetNewLocation()
        {
            Vector3 newLocation;

            newLocation = Random.insideUnitSphere * _targetRadius;
            newLocation.x += _player.transform.position.x;
            newLocation.y = transform.position.y;
            newLocation.z += _player.transform.position.z;

            return newLocation;
        }
    }
}

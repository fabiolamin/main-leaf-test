using System.Collections;
using UnityEngine;
using Scripts.Player;
using UnityEngine.AI;
using Scripts.Utils;

namespace Scripts.Enemy.States.Archer
{
    public sealed class EnemyArcherAttackState : BaseState
    {
        [SerializeField] private NavMeshAgent _enemyAgent;
        [SerializeField] private float _attackDuration = 3f;

        private PlayerMovement _player;
        private bool _isReadyToAttack = false;

        public override void SetUpState()
        {
            _isReadyToAttack = true;
            _player = FindObjectOfType<PlayerMovement>();
        }

        public override void UpdateState()
        {
            _enemyAgent.transform.LookAt(_player.transform);

            if (Physics.Raycast(_enemyAgent.transform.position, _enemyAgent.transform.forward, 5f, 0))
            {
                _stateMachine.ChangeState(nextState);
            }
            else
            {
                StartCoroutine(StartAttack());
            }
        }

        private IEnumerator StartAttack()
        {
            if (_isReadyToAttack)
            {
                _enemyAgent.isStopped = true;
                _isReadyToAttack = false;

                yield return new WaitForSeconds(_attackDuration);

                _stateMachine.ChangeState(nextState);
            }
        }
    }
}


using UnityEngine;
using UnityEngine.AI;
using Scripts.Utils;

namespace Scripts.Enemy.States.Archer
{
    public sealed class EnemyMoveState : BaseState
    {
        [SerializeField] private NavMeshAgent _enemyAgent;

        public override void SetUpState()
        {
            _enemyAgent.isStopped = false;
        }

        public override void UpdateState()
        {
            if (_enemyAgent.remainingDistance <= _enemyAgent.stoppingDistance)
            {
                _stateMachine.ChangeState(nextState);
            }
        }
    }
}


using UnityEngine;

namespace Scripts.Enemy
{
    public sealed class EnemySystem : MonoBehaviour
    {
        [SerializeField] private EnemyManager _enemyManager;

        private void Awake()
        {
            _enemyManager.OnAwake();
        }

        private void Update()
        {
            _enemyManager.OnUpdate();
        }
    }
}


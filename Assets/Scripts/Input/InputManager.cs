using UnityEngine;

namespace Scripts.Input
{
    public sealed class InputManager : MonoBehaviour
    {
        public delegate void MovementAxisUpdatedHandler(Vector2 axis);
        public event MovementAxisUpdatedHandler OnMovementAxisUpdated;

        public delegate void JumpButtonUpdatedHandler(bool wasJumpButtonPressed);
        public event JumpButtonUpdatedHandler OnJumpButtonPressed;

        public delegate void ShootButtonUpdatedHandler(bool wasShootButtonPressed);
        public event ShootButtonUpdatedHandler OnShootButtonPressed;

        public InputControls InputControls { get; private set; }
        public Vector2 MovementAxis => InputControls.Player.Movement.ReadValue<Vector2>();
        public Vector2 MouseDelta => InputControls.Player.Look.ReadValue<Vector2>();
        public bool WasJumpButtonPressed => InputControls.Player.Jump.triggered;
        public bool WasShootButtonPressed => InputControls.Player.Shoot.triggered;

        public void OnUpdate()
        {
            OnMovementAxisUpdated?.Invoke(MovementAxis);
            OnJumpButtonPressed?.Invoke(WasJumpButtonPressed);
            OnShootButtonPressed?.Invoke(WasShootButtonPressed);
        }

        private void OnEnable()
        {
            InputControls = new InputControls();
            InputControls.Enable();
            Cursor.visible = false;
        }

        private void OnDisable()
        {
            InputControls.Disable();
        }
    }
}

